﻿using System;
using RabbitMQ.Client;
using System.Text;
using System.Timers;
using System.Collections.Generic;

namespace Send
{
    class Send
    {
        private static Timer aTimer;
        static void Main(string[] args)
        {
            aTimer = new System.Timers.Timer();
            aTimer.Interval = 10;
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
            Console.WriteLine("Press the Enter key to exit the program at any time... ");
            Console.ReadLine();
        }
        private static void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            var random = new Random();
            var endPointList = new List<AmqpTcpEndpoint>
            {
                new AmqpTcpEndpoint("localhost", 5672),
                new AmqpTcpEndpoint("localhost", 5673),
                new AmqpTcpEndpoint("localhost", 5674),
                //new AmqpTcpEndpoint("localhost", 5675),
                //new AmqpTcpEndpoint("localhost", 5676),
            };
            var factory = new ConnectionFactory();
            
            using (var connection = factory.CreateConnection(endPointList))
            using (var channel = connection.CreateModel())
            {
                int i = 1;
                var result = random.Next(101);
                var routingKey = result % 2 == 0 ? "sample1" : "sample2" ;
                string message = "Random number : " + result.ToString();
                i++;
                var body = Encoding.UTF8.GetBytes(message);
                // channel.BasicPublish(exchange: "e_rabbit1",
                //                      routingKey: "simple1",
                //                      basicProperties: null,
                //                      body: body);
                PublicationAddress address = new PublicationAddress("topic", "e_cluster", routingKey);
                channel.BasicPublish(addr: address,
                                     basicProperties: null,
                                     body: body);
                Console.WriteLine(" [x] Sent {0}", message);
            }
        }
    }
}
