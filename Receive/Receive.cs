﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Receive
{
    class Receive
    {
        public static void Main()
        {
            var endPointList = new List<AmqpTcpEndpoint>
            {
                new AmqpTcpEndpoint("localhost", 5672),
                new AmqpTcpEndpoint("localhost", 5673),
                new AmqpTcpEndpoint("localhost", 5674),
                //new AmqpTcpEndpoint("localhost", 5675),
                //new AmqpTcpEndpoint("localhost", 5676),
            };
            var factory = new ConnectionFactory();
            
            using (var connection = factory.CreateConnection(endPointList))
            using (var channel = connection.CreateModel())
            {
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [x] Received Que 1: {0}", message);
                };
                try
                {
                    channel.BasicConsume(queue: "q_que_1",
                                                         autoAck: true,
                                                         consumer: consumer);
                    Console.WriteLine(" Press [enter] to exit.");
                    Console.ReadLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
            }
        }
    }
}